const fs = require('then-fs');
const fs1 = require('fs');
const path = require('path');

const palettesFolder = path.join(process.cwd(), 'palettes');
const palettesFilePath = path.join(process.cwd(), '_palettes.scss');
const baseContrast = `\tcontrast: (
    \t50 : $black,
    \t100 : $black,
    \t200 : $white,
    \t300 : $white,
    \t400 : $white,
    \t500 : $white,
    \t600 : $white,
    \t700 : $white,
    \t800 : $white,
    \t900 : $white
    )`;

const writePalette = (name, base, pal) => {
    const fileName = `${name}-palette.scss`;
    const filePath = path.join(palettesFolder, fileName);
    const colors = [];

    if (fs.existsSync(filePath)){
        fs.unlinkSync(filePath);
    }   
    
    for (const key in pal) {
        if (pal.hasOwnProperty(key)) {
            const color = pal[key];
            colors.push(`\t${key} : #${color}`);
        }
    }
    colors.push(baseContrast);

    let contents = `//palette is generated based on #${base} color\n`;
    contents += `$${name}-palette: (\n`;
    contents += colors.join(',\n');
    contents += '\n);';

    return fs.writeFile(filePath, contents)
        .then(() => {
            return fs.readFile(palettesFilePath);
        })
        .then(data => {
            const importStatement = `@import 'palettes/${name}-palette';`;
            if (data.indexOf(importStatement) < 0) {
                data += '\n' + importStatement;
                return fs.writeFile(palettesFilePath, data);
            }
            return true;
        });
}

module.exports = {
    writePalette
};
