#!/usr/bin/env node

const program = require('commander');
const { prompt } = require('inquirer');

const { generatePalette } = require('./palette-generator');
const { writePalette } = require('./palette-writer');

const questions = [
    {
      type : 'input',
      name : 'name',
      message : 'Enter palette name ...'
    },
    {
      type : 'input',
      name : 'baseColor',
      message : 'Enter base color ...'
    }
]; 

function generate(palette) {
  generatePalette(palette.baseColor)
    .then(res => {
      console.log(`Generated ${palette.name} palette for base color #${palette.baseColor}:`);
      console.log(JSON.stringify(res))
      return writePalette(palette.name, palette.baseColor, res);  
    })
    .then(res => {
      console.log('Palette written. SUCCESS.');
    });
}

program
  .version('0.0.1')
  .description('Palette generation');

program
  .command('addPalette')
  .alias('a')
  .description('Generate a palette for base color')
  .action(() => {
    prompt(questions).then(answers => generate(answers));
  });

program
  .command('generatePalette <name> <base-color>')
  .alias('g')
  .description('Generate a palette for base color')
  .action((name, baseColor) => {
       generate({name,baseColor})
  });

program.parse(process.argv);