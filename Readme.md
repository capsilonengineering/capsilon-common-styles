# CAPSILON COMMON STYLES  

This is a styles library designed to provide a common UI components through the entire Capsilon. It contains either source files for styles generation and tools for applying material principles.

## How does it work?

The library is based on the [Angular Material](https://material.angular.io/) project and applies [Material](https://material.io/) principles for UI implementation.
 
### Generating themes

Common styles generation is based on [Angular Material Theming](https://material.angular.io/guide/theming).

- theme can be built using **angular-material-theme** mixin

    ```
    @include angular-material-theme($capsilon-theme);
    ```

- as alternative a theme consisting of a subset of components can be build or using component specific mixins (i.e. **mat-button-theme**, **mat-icon-theme**, etc.)
- for a prebuilt theme sass transpiler should be used

    ```
    node-sass capsilon-theme.scss dist/capsilon-theme.css
    ```

- theme embedding depends on the application build process and can vary, if a theming guideline is used prebuilt theme should be embedded as pure css

    ```
    <link href="capsilon-common-styles/capsilon-theme.css" rel="stylesheet">
    ```

- for custom components styling use [Angular Material Components](https://material.angular.io/guide/theming-your-components) guide.

### Generating palettes

All the theming requires palettes with color grade to be prepared for. For the palette generation the CLI is introduces based the [Material Color Tool](https://material.io/inline-tools/color/). 

- in order to generate a palette execute the following steps
    * run the following command from commnd line tool

        ```
        node palette-cli.js [a|addPalette]
        ```

    * specify the name of the palette (i.e. primary)

        ```
        ? Enter palette name ... primary
        ```

    * specify palettes pase color in hex format (i.e. 3990d8)

        ```
        ? Enter base color ... 3990d8
        ```

    * alternativaly the following command line with arguments specifying name and base color can be used

        ```
        node palette-cli.js [g|generatePalette] primary 3990d8
        ```

- after tool is run the palette specific file is generated or reqritten in *palletes* folder and added as a reference for theme generation into *_palettes.scss* file
- for palette-based theme generation the *default*, *lighter*, and *darker* colors from the palette grades should be specified by styles developer/designer (the defaults are 500, 100 and 700) and passed to *mat-palette* angular material function in *capsilon-theme.scss* file.

    ```
    $capsilon-primary:  mat-palette($primary-palette, 500, 300, 900);
    ```

- the values for base component styles (i.e. default button in capsilon comomn styles demo) can be specified in **$mat-light-theme-background** and **$mat-light-theme-foreground** variables in the same file.

## How do I get set up?
  
- just run npm install command

    ```
	[sudo] npm install
    ```